#define CASH_DRAWER 1

uint8_t last = 0;

void setup() {
  pinMode(CASH_DRAWER, OUTPUT);

  Serial.Begin(9600);

}

void loop() {
  if (Serial.available() > 0) {
    uint8_t in = Serial.read();
    if (in != last) {
      digitalWrite(CASH_DRAWER, HIGH);
      serialWrite(in);
      last = in;
      delay(200);
      digitalWrite(CASH_DRAWER, LOW);
    }
  }
    
}
